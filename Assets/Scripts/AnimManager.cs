﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AnimManager : MonoBehaviour
{
    public Animator animator;
    //public int animBase;
    //public int animLayer;

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        animator.SetInteger("animBaseInt", 1);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void animBaseChange()
    {
        var clickedButton = EventSystem.current.currentSelectedGameObject.name;

        if (clickedButton == "Idle_Standing")
        {
            animator.Play("animBaseInt");
        }
        else if (clickedButton == "Angry_01")
        {
            animator.Play("animBaseInt", 2);
        }
    }
}
