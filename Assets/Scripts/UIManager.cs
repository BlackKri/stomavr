﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    [Header("TissueBox UI")]
    public Button tissueBoxInfoBTN;
    public Button tissueBoxBackBTN;
    public GameObject tissueBoxTitlePanel;
    public GameObject tissueBoxTextPanel;

    [Header("Tray UI")]
    public Button trayInfoBTN;
    public Button trayBackBTN;
    public GameObject trayTitlePanel;
    public GameObject trayTextPanel;

    [Header("Glove Box UI")]
    public Button gloveBoxInfoBTN;
    public Button gloveBoxBackBTN;
    public GameObject gloveBoxTitlePanel;
    public GameObject gloveBoxTextPanel;

    [Header("Scissors UI")]
    public Button scissorsInfoBTN;
    public Button scissorsBackBTN;
    public GameObject scissorsTitlePanel;
    public GameObject scissorsTextPanel;

    [Header("Stoma Paste UI")]
    public Button stomaPasteInfoBTN;
    public Button stomaPasteBackBTN;
    public GameObject stomaPasteTitlePanel;
    public GameObject stomaPasteTextPanel;

    [Header("Stoma Pouch UI")]
    public Button stomaPouchInfoBTN;
    public Button stomaPouchBackBTN;
    public GameObject stomaPouchTitlePanel;
    public GameObject stomaPouchTextPanel;

    [Header("Stoma BasePlate UI")]
    public Button stomaBasePlateInfoBTN;
    public Button stomaBasePlateBackBTN;
    public GameObject stomaBasePlateTitlePanel;
    public GameObject stomaBasePlateTextPanel;

    [Header("Stoma Patient UI")]
    public Button stomaPatientInfoBTN;
    public Button stomaPatientBackBTN;
    public GameObject stomaPatientTitlePanel;
    public GameObject stomaPatientTextPanel;

    [Header("Laptop/Main Menu - MainMenu UI")]
    public Button stage1Btn;
    public Button stage2Btn;
    //public Button aboutBTN;
    //public Button optionBTN;
    public Button exitBtn;

    [Header("Laptop/Main Menu - Stage 1 UI")]
    //public GameObject stageInfoPanel1;
    //public Text instructText1;
    public Button resetStage1BTN;
    public Button practiceBTN;
    public Button backToMainMenuBtn;
    //[SerializeField] bool isMenuOpen1;
    //[SerializeField] bool isResetTouch;
    //[SerializeField] bool isPracticeTouch;
    //[SerializeField] bool isAboutTouch;
    //[SerializeField] bool isRestartTouch;
    //[SerializeField] bool isExitTouch;

    [Header("Laptop/Main Menu - Stage 2 UI")]
    //public GameObject stageInfoPanel2;
    public Text instructText2;
    public Button previousBTN;
    public Button nextBTN;
    public Button endPracticeBTN;
    public Button backToMainMenu2Btn;
    public Button openMenuBTN;
    public Button closeMenuBTN;
    [SerializeField] bool isMenuOpen;
    //[SerializeField] bool isPreviousTouch;
    //[SerializeField] bool isNextTouch;
    //[SerializeField] bool isEndPracticeTouch;

    [Header("Laptop/Main Menu - Final Results UI")]
    //public GameObject stageInfoPanel3;
    //public GameObject resultMistakesPanel;
    //public GameObject resultImplictionPanel3;
    //public GameObject resultCorrectionsPanel3;
    //public Text instructText3;
    public Button resetPracticeBtn;
    public Button backToMainMenu3Btn;

    [Header("TipBox - TissueBox")]
    public GameObject tipboxPanel1;
    public Text tipText1;
    public Button close1Btn;

    [Header("TipBox1 - Stoma BasePlate")]
    public GameObject tipboxPanel2;
    public Text tipText2;
    public Button close2Btn;

    [Header("TipBox2 - Stoma BasePlate")]
    public GameObject tipboxPanel3;
    public Text tipText3;
    public Button close3Btn;

    [Header("TipBox3 - Stoma BasePlate")]
    public GameObject tipboxPanel4;
    public Text tipText4;
    public Button close4Btn;

    [Header("TipBox4 - Stoma Patient")]
    public GameObject tipboxPanel5;
    public Text tipText5;
    public Button close5Btn;

    [Header("TipBox - Stoma Pouch")]
    public GameObject tipboxPanel6;
    public Text tipText6;
    public Button close6Btn;

    ObjectManager objm;
    FadeManager fm;

    //#region Singleton
    //private static UIManager _instance;

    //public static UIManager Instance
    //{
    //    get
    //    {
    //        if (_instance == null)
    //        {
    //            _instance = GameObject.FindObjectOfType<UIManager>();
    //        }

    //        return _instance;
    //    }
    //}
    //#endregion

    //void Awake()
    //{
    //    DontDestroyOnLoad(gameObject);
    //}

    // Start is called before the first frame update
    void Start()
    {
        //Laptop - Main Menu aka [Scene 1] UI Setup
        stage1Btn.gameObject.SetActive(false);
        stage2Btn.gameObject.SetActive(false);
        //aboutBTN.gameObject.SetActive(false);
        //optionBTN.gameObject.SetActive(false);

        //Laptop - Stage 1 aka [Scene 2] UI Setup
        //stageInfoPanel1.gameObject.SetActive(true);
        //instructText1.gameObject.SetActive(true);
        resetStage1BTN.gameObject.SetActive(false);
        practiceBTN.gameObject.SetActive(false);
        backToMainMenuBtn.gameObject.SetActive(false);
        //isMenuOpen1 = false;
        //isPracticeTouch = false;
        //isEndPracticeTouch = false;
        //isAboutTouch = false;
        //isRestartTouch = false;
        //isExitTouch = false;

        //Laptop - Stage 2 aka [Scene 3] UI Setup
        //stageInfoPanel2.gameObject.SetActive(true);
        instructText2.gameObject.SetActive(true);
        previousBTN.gameObject.SetActive(false);
        nextBTN.gameObject.SetActive(false);
        endPracticeBTN.gameObject.SetActive(false);
        backToMainMenu2Btn.gameObject.SetActive(false);
        //openMenuBTN.gameObject.SetActive(false);
        //closeMenuBTN.gameObject.SetActive(true);
        isMenuOpen = false;

        //Laptop - Final Results 3 aka [Scene 4] UI Setup
        //stageInfoPanel3.gameObject.SetActive(true);
        //instructText3.gameObject.SetActive(true);
        resetPracticeBtn.gameObject.SetActive(false);
        backToMainMenu3Btn.gameObject.SetActive(false);


        //Tissue Box UI Setup
        tissueBoxTextPanel.gameObject.SetActive(false);
        tissueBoxTitlePanel.gameObject.SetActive(false);
        tissueBoxInfoBTN.gameObject.SetActive(true);
        tissueBoxBackBTN.gameObject.SetActive(false);

        //Tray UI Setup
        trayTextPanel.gameObject.SetActive(false);
        trayTitlePanel.gameObject.SetActive(false);
        trayInfoBTN.gameObject.SetActive(true);
        trayBackBTN.gameObject.SetActive(false);

        //Glove Box UI Setup
        gloveBoxTextPanel.gameObject.SetActive(false);
        gloveBoxTitlePanel.gameObject.SetActive(false);
        gloveBoxInfoBTN.gameObject.SetActive(true);
        gloveBoxBackBTN.gameObject.SetActive(false);

        //Scissors UI Setup
        scissorsTextPanel.gameObject.SetActive(false);
        scissorsTitlePanel.gameObject.SetActive(false);
        scissorsInfoBTN.gameObject.SetActive(true);
        scissorsBackBTN.gameObject.SetActive(false);

        //Stoma Paste UI Setup
        stomaPasteTextPanel.gameObject.SetActive(false);
        stomaPasteTitlePanel.gameObject.SetActive(false);
        stomaPasteInfoBTN.gameObject.SetActive(true);
        stomaPasteBackBTN.gameObject.SetActive(false);

        //Stoma Pouch UI Setup
        stomaPouchTextPanel.gameObject.SetActive(false);
        stomaPouchTitlePanel.gameObject.SetActive(false);
        stomaPouchInfoBTN.gameObject.SetActive(true);
        stomaPouchBackBTN.gameObject.SetActive(false);

        //Stoma Base Plate UI Setup
        stomaBasePlateTextPanel.gameObject.SetActive(false);
        stomaBasePlateTitlePanel.gameObject.SetActive(false);
        stomaBasePlateInfoBTN.gameObject.SetActive(true);
        stomaBasePlateBackBTN.gameObject.SetActive(false);

        //Stoma Patient UI Setup
        stomaPatientTextPanel.gameObject.SetActive(false);
        stomaPatientTitlePanel.gameObject.SetActive(false);
        stomaPatientInfoBTN.gameObject.SetActive(true);
        stomaPatientBackBTN.gameObject.SetActive(false);

        //TipBox - TissueBox UI Setup
        tipboxPanel1.gameObject.SetActive(false);
        tipText1.gameObject.SetActive(false);
        close1Btn.gameObject.SetActive(false);

        //TipBox - BasePlate UI Setup
        tipboxPanel2.gameObject.SetActive(false);
        tipText2.gameObject.SetActive(false);
        close2Btn.gameObject.SetActive(false);

        tipboxPanel3.gameObject.SetActive(false);
        tipText3.gameObject.SetActive(false);
        close3Btn.gameObject.SetActive(false);

        tipboxPanel4.gameObject.SetActive(false);
        tipText4.gameObject.SetActive(false);
        close4Btn.gameObject.SetActive(false);

        tipboxPanel5.gameObject.SetActive(false);
        tipText5.gameObject.SetActive(false);
        close5Btn.gameObject.SetActive(false);

        //TipBox - Stoma Pouch UI Setup
        tipboxPanel6.gameObject.SetActive(false);
        tipText6.gameObject.SetActive(false);
        close6Btn.gameObject.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////// Scene 1  Methods Starts Here /////////////////////////////
    public void GoToScene2FromScene1()
    {
        fm.FadeToScene(1);
        //stage1Btn.gameObject.SetActive(false);
        //stage2Btn.gameObject.SetActive(false);
        //    aboutBTN.gameObject.SetActive(false);
        //    optionBTN.gameObject.SetActive(false);
    }
    public void GoToScene3FromScene1()
    {
        fm.FadeToScene(2);
        //stage1Btn.gameObject.SetActive(false);
        //stage2Btn.gameObject.SetActive(false);
        //aboutBTN.gameObject.SetActive(false);
        //optionBTN.gameObject.SetActive(false);
    }

    //public void AboutScreen()
    //{

    //}

    //public void OptionsScreen()
    //{

    //}

    public void QuitGame()
    {
        Application.Quit();
    }

    ///////////////////////////// Scene 1  Methods Ends Here ///////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////// Scene 2  Methods Starts Here /////////////////////////////
    public void TissueBoxInfoActive1()
    {
        tissueBoxTextPanel.gameObject.SetActive(false);
        tissueBoxTitlePanel.gameObject.SetActive(false);
        tissueBoxInfoBTN.gameObject.SetActive(true);
        tissueBoxBackBTN.gameObject.SetActive(false);
    }
    public void TissueBoxInfoActive2()
    {
        tissueBoxTextPanel.gameObject.SetActive(true);
        tissueBoxTitlePanel.gameObject.SetActive(true);
        tissueBoxInfoBTN.gameObject.SetActive(false);
        tissueBoxBackBTN.gameObject.SetActive(true);
    }
    
    public void TrayInfoActive1()
    {
        trayTextPanel.gameObject.SetActive(true);
        trayTitlePanel.gameObject.SetActive(true);
        trayInfoBTN.gameObject.SetActive(false);
        trayBackBTN.gameObject.SetActive(true);
    }
    public void TrayInfoActive2()
    {
        trayTextPanel.gameObject.SetActive(false);
        trayTitlePanel.gameObject.SetActive(false);
        trayInfoBTN.gameObject.SetActive(true);
        trayBackBTN.gameObject.SetActive(false);
    }

    public void GloveBoxInfoActive1()
    {
        gloveBoxTextPanel.gameObject.SetActive(true);
        gloveBoxTitlePanel.gameObject.SetActive(true);
        gloveBoxInfoBTN.gameObject.SetActive(false);
        gloveBoxBackBTN.gameObject.SetActive(true);
    }
    public void GloveBoxInfoActive2()
    {
        gloveBoxTextPanel.gameObject.SetActive(false);
        gloveBoxTitlePanel.gameObject.SetActive(false);
        gloveBoxInfoBTN.gameObject.SetActive(true);
        gloveBoxBackBTN.gameObject.SetActive(false);
    }
    public void ScissorsInfoActive1()
    {
        scissorsTextPanel.gameObject.SetActive(true);
        scissorsTitlePanel.gameObject.SetActive(true);
        scissorsInfoBTN.gameObject.SetActive(false);
        scissorsBackBTN.gameObject.SetActive(true);
    }
    public void ScissorsInfoActive2()
    {
        scissorsTextPanel.gameObject.SetActive(false);
        scissorsTitlePanel.gameObject.SetActive(false);
        scissorsInfoBTN.gameObject.SetActive(true);
        scissorsBackBTN.gameObject.SetActive(false);
    }
    public void StomaPasteInfoActive1()
    {
        stomaPasteTextPanel.gameObject.SetActive(true);
        stomaPasteTitlePanel.gameObject.SetActive(true);
        stomaPasteInfoBTN.gameObject.SetActive(false);
        stomaPasteBackBTN.gameObject.SetActive(true);
    }
    public void StomaPasteInfoActive2()
    {
        stomaPasteTextPanel.gameObject.SetActive(false);
        stomaPasteTitlePanel.gameObject.SetActive(false);
        stomaPasteInfoBTN.gameObject.SetActive(true);
        stomaPasteBackBTN.gameObject.SetActive(false);
    }
    public void StomaPouchInfoActive1()
    {
        stomaPouchTextPanel.gameObject.SetActive(true);
        stomaPouchTitlePanel.gameObject.SetActive(true);
        stomaPouchInfoBTN.gameObject.SetActive(false);
        stomaPouchBackBTN.gameObject.SetActive(true);
    }
    public void StomaPouchInfoActive2()
    {
        stomaPouchTextPanel.gameObject.SetActive(false);
        stomaPouchTitlePanel.gameObject.SetActive(false);
        stomaPouchInfoBTN.gameObject.SetActive(true);
        stomaPouchBackBTN.gameObject.SetActive(false);
    }
    public void StomaBasePatientInfoActive1()
    {
        stomaBasePlateTextPanel.gameObject.SetActive(true);
        stomaBasePlateTitlePanel.gameObject.SetActive(true);
        stomaBasePlateInfoBTN.gameObject.SetActive(false);
        stomaBasePlateBackBTN.gameObject.SetActive(true);
    }
    public void StomaBasePatientInfoActive2()
    {
        stomaBasePlateTextPanel.gameObject.SetActive(false);
        stomaBasePlateTitlePanel.gameObject.SetActive(false);
        stomaBasePlateInfoBTN.gameObject.SetActive(true);
        stomaBasePlateBackBTN.gameObject.SetActive(false);
    }
    public void StomaPatientInfoActive1()
    {
        stomaPatientTextPanel.gameObject.SetActive(true);
        stomaPatientTitlePanel.gameObject.SetActive(true);
        stomaPatientInfoBTN.gameObject.SetActive(false);
        stomaPatientBackBTN.gameObject.SetActive(true);
    }
    public void StomaPatientInfoActive2()
    {
        stomaPatientTextPanel.gameObject.SetActive(false);
        stomaPatientTitlePanel.gameObject.SetActive(false);
        stomaPatientInfoBTN.gameObject.SetActive(true);
        stomaPatientBackBTN.gameObject.SetActive(false);
    }

    //public void MenuOpen1()
    //{
    //    if(!isMenuOpen1)
    //    {
    //        resetStage1BTN.gameObject.SetActive(true);
    //        practiceBTN.gameObject.SetActive(true);
    //        backToMainMenuBtn.gameObject.SetActive(true);
    //        openMenuBTN.gameObject.SetActive(true);
    //        closeMenuBTN.gameObject.SetActive(false);
    //        isMenuOpen1 = true;
    //    }
    //}

    //public void MenuClose1()
    //{
    //    if (isMenuOpen1)
    //    {
    //        resetStage1BTN.gameObject.SetActive(false);
    //        practiceBTN.gameObject.SetActive(false);
    //        backToMainMenuBtn.gameObject.SetActive(false);
    //        openMenuBTN.gameObject.SetActive(false);
    //        closeMenuBTN.gameObject.SetActive(true);
    //        isMenuOpen1 = false;
    //    }
    //}
    public void ResetScene() // <-- Reload Scene 2
    {
        SceneManager.LoadScene(1);
    }

    public void GoToScene3FromScene2() // <-- Go to Practice Mode in Scene 3
    {
        fm.FadeToScene(2);
    }
    public void GoToScene1FromScene2() // <-- Go to Main Menu in Scene 1
    {
        fm.FadeToScene(2);
    }

    ///////////////////////////// Scene 2  Methods Ends Here ///////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////// Scene 3  Methods Starts Here /////////////////////////////
    public void TipBoxTissue()
    {
        
    }

    public void MenuClose()
    {
        
    }

    public void GoToScene2FromScene3() // <-- Back to Learning Mode in Scene 2
    {
        fm.FadeToScene(1);
    }
    public void GoToScene1FromScene3() // <-- Go to Main Menu in Scene 4
    {
        fm.FadeToScene(3);
    }

    ///////////////////////////// Scene 3  Methods Ends Here ///////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////// Scene 4  Methods Starts Here /////////////////////////////
    public void GoToScene1FromScene4() // <-- Go to Main Menu in Scene 1
    {
        fm.FadeToScene(0);
    }

    public void GoToScene2FromScene4() // <-- Go to Learning Mode in Scene 2
    {
        fm.FadeToScene(1);
    }

    ///////////////////////////// Scene 4  Methods Ends Here ///////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
}
