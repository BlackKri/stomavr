﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] GameObject TissueBox;
    [SerializeField] GameObject TrayBox;
    [SerializeField] GameObject GloveBox;
    [SerializeField] GameObject Scissor;
    [SerializeField] BoxCollider TissueBoxCol;
    [SerializeField] BoxCollider TrayCol;
    [SerializeField] BoxCollider GloveCol;
    [SerializeField] BoxCollider ScissorCol;
    [SerializeField] bool isTissueBoxTouch;
    [SerializeField] bool isTrayTouch;
    [SerializeField] bool isGloveBoxTouch;
    [SerializeField] bool isScissorsTouch;
    [SerializeField] bool isPracticeTouch;
    [SerializeField] bool isAboutTouch;
    [SerializeField] bool isRestartTouch;
    [SerializeField] bool isExitTouch;

    UIManager ui;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //void OnCollisionEnter(Collider other) 
    //{
    //   if(!isTissueBoxTouch)
    //   {
    //        isTissueBoxTouch = true;
    //        //ui.TissueBoxInfo();
    //   }
    //   else
    //   {
    //        isTissueBoxTouch = false;
    //   }
    //}
}
