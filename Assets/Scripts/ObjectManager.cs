﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectManager : MonoBehaviour
{
    //[SerializeField] public GameObject TrayBox;
    //[SerializeField] public GameObject GloveBox;
    //[SerializeField] public GameObject Scissor;
    public BoxCollider TissueBoxCol;
    public BoxCollider TrayCol;
    public BoxCollider GloveCol;
    public BoxCollider ScissorCol;
    public BoxCollider LaptopBoxCol;
    public bool isTissueBoxTouch;
    public bool isTrayTouch;
    public bool isGloveBoxTouch;
    public bool isScissorsTouch;
    public bool isLaptopTouch;

    UIManager ui;
    //#region Singleton
    //private static ObjectManager _instance;

    //public static ObjectManager Instance
    //{
    //    get
    //    {
    //        if (_instance == null)
    //        {
    //            _instance = GameObject.FindObjectOfType<ObjectManager>();
    //        }

    //        return _instance;
    //    }
    //}
    //#endregion

    //void Awake()
    //{
    //    DontDestroyOnLoad(gameObject);
    //}

    // Start is called before the first frame update
    void Start()
    {
        isTissueBoxTouch = false;
        isTrayTouch = false;
        isGloveBoxTouch = false;
        isScissorsTouch = false;
        isLaptopTouch = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        // Tissue Box
        if (other.tag == "Line" && !isTissueBoxTouch)
        {
            isTissueBoxTouch = true;
            TissueBoxCol.gameObject.SetActive(true);
            Debug.Log("Cursor entered");
        }
        // Laptop
        if (other.tag == "Line" && !isLaptopTouch)
        {
            isLaptopTouch = true;
            LaptopBoxCol.gameObject.SetActive(true);
            Debug.Log("Cursor entered");
        }

    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Line" && isTissueBoxTouch)
        {
            isTissueBoxTouch = false;
            TissueBoxCol.gameObject.SetActive(false);
            Debug.Log("Cursor exited");
        }

        if (other.tag == "Line" && isLaptopTouch)
        {
            isLaptopTouch = false;
            LaptopBoxCol.gameObject.SetActive(false);
            Debug.Log("Cursor exited");
        }
    }
}
